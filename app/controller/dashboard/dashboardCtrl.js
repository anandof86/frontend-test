(function() {
    //'use strict';
    angular
        .module('activity')
        .controller('dashboardCtrl', dashboardCtrl);

    dashboardCtrl.$inject = ['$scope','$state','dataservice']

    function dashboardCtrl($scope, $state, dataservice) {
      //Get Activity Data
      $scope.linelabel = [];
      $scope.linedata = [];
      $scope.labels = [];
      $scope.data = [];
      $scope.colors = ['#c25991', '#f9992e', '#5ddfca', '#a578f7'];
      $scope.doughnut_label = [];
      $scope.doughnut_data = [];
      dataservice.getactivity().then(function(data){
         $scope.activity_data = data.data;
         for(var i in $scope.activity_data){
            //Chart Speed
            $scope.linelabel.push(moment.unix($scope.activity_data[i].data.time).format("HH:mm"))
            $scope.linedata.push($scope.activity_data[i].data.speed);
            //Chart Speed Average
            $scope.doughnut_label.push($scope.activity_data[i].zoneId);
            $scope.doughnut_data.push($scope.activity_data[i].data.speed/$scope.activity_data[i].data.count);
            //Chart Count
            $scope.labels.push($scope.activity_data[i].zoneId);
            $scope.data.push($scope.activity_data[i].data.count);
         }
      });


    }

})();
