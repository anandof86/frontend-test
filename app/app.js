(function() {
    'use strict';

    angular.module('activity', ['ui.router','chart.js'])

        .config(config)
        .run(init);

        config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

        function config($stateProvider, $urlRouterProvider, $locationProvider){
            $locationProvider.hashPrefix('');

            $stateProvider.state('dashboard', {
                url:'/',
                views: {
                    'header@': {
                        templateUrl: 'views/layout/header.html'
                    },
                    'leftmenu@': {
                        templateUrl: 'views/layout/sidebar.html',
                    },
                    'content@': {
                        templateUrl : 'views/dashboard/dashboard.html',
                        controller : 'dashboardCtrl'
                    }
                }
            }).state('news', {
                url:'/news',
                views: {
                    'header@': {
                        templateUrl: 'views/layout/header.html'
                    },
                    'leftmenu@': {
                        templateUrl: 'views/layout/sidebar.html',
                    },
                    'content@': {
                        templateUrl : 'views/news/news.html',
                        controller : 'newsCtrl'
                    }
                }
            });
            $urlRouterProvider.otherwise("/");
        }

        init.$inject = ['$rootScope','$location','$log'];

        function init($rootScope,$location,$log){


        }

})();
