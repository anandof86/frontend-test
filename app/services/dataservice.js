(function() {
    //'use strict';
    angular
        .module('activity')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$q','$http','$state']

    function dataservice($q,$http,$state) {
        return {
            getactivity: function() {
                var deferred = $q.defer();
                $http.get('static/activity-data.json').then(function(data){
                   deferred.resolve(data);
                },function(error){
                    deferred.reject();
                });
                return deferred.promise;
            }
        }
    }

})();
